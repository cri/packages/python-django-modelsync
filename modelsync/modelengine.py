from .engine import UnidirectionalSyncEngine


class RemoteModelSyncMixin:
    def __init__(self, *args, local_model, remote_model, **kwargs):
        super().__init__(
            *args, local_model=local_model, remote_model=remote_model, **kwargs
        )
        self.local_model = local_model
        self.remote_model = remote_model

    def get_remote_obj(self, remote_obj_id):
        return self.remote_model.objects.get(pk=remote_obj_id)

    def add_remote_obj(self, _local_obj_id, local_obj_data, *, pretend=False):
        o = self.remote_model(
            **{k: v for k, v in local_obj_data.items() if hasattr(self.remote_model, k)}
        )
        if not pretend:
            o.save(force_insert=True)
        return o

    def update_obj(
        self,
        local_obj_id,
        local_obj_data,
        remote_obj_id,
        remote_obj_data,
        *,
        pretend=False,
    ):
        changes = self.get_obj_diff(
            local_obj_id, local_obj_data, remote_obj_id, remote_obj_data
        )
        if changes:
            remote_obj = self.get_remote_obj(remote_obj_id)
            for attr, (_old, new) in changes.items():
                setattr(remote_obj, attr, new)
            if not pretend:
                remote_obj.save(force_update=True)
        return changes

    def delete_remote_obj(self, remote_obj_id, *, pretend=False):
        o = self.get_remote_obj(remote_obj_id)
        if not pretend:
            o.delete()


class SyncRemoteModelEngine(RemoteModelSyncMixin, UnidirectionalSyncEngine):
    pass


class SyncedModelMixin:
    def sync(self, *, pretend=False):
        result = {}
        for adapter in self.get_sync_adapters():
            result[adapter] = adapter(self).sync(pretend=pretend)
        return result

    @classmethod
    def get_sync_adapters(cls):
        return cls.SyncMeta.sync_adapters

    @classmethod
    def partial_sync(cls, local_objects, *, pretend=False):
        result = {}
        for adapter in cls.get_sync_adapters():
            result[adapter] = adapter.partial_sync(local_objects, pretend=pretend)
        return result

    @classmethod
    def sync_all(cls, *, pretend=False):
        result = {}
        for adapter in cls.get_sync_adapters():
            result[adapter] = adapter.sync_all(model=cls, pretend=pretend)
        return result


class SyncedModelAdapter:
    sync_engine_class = SyncRemoteModelEngine
    sync_local_queryset = None
    sync_remote_model = None
    sync_remote_queryset = None

    def __init__(self, obj):
        self.obj = obj

    def sync(self, *, pretend=False):
        return self.partial_sync([self.obj], pretend=pretend)

    def get_sync_local_obj_id_multi(self):
        yield self.sync_local_obj_id()

    def get_sync_local_obj_id(self):
        return self.obj.pk

    def get_sync_remote_obj_id_multi(self, remote_objects):
        yield (
            self.get_sync_local_obj_id(),
            self.get_sync_remote_obj_id(remote_objects),
        )

    def get_sync_remote_obj_id(self, remote_objects):
        raise NotImplementedError

    def get_sync_local_data_multi(self):
        yield self.get_sync_local_data()

    def get_sync_local_data(self):
        raise NotImplementedError

    @staticmethod
    def get_sync_remote_data(remote_obj):
        raise NotImplementedError

    @classmethod
    def get_sync_objects_map(cls, local_objects, remote_objects):
        obj_map = {}
        for local_obj in local_objects:
            mapping = local_obj.get_sync_remote_obj_id_multi(remote_objects)
            for local_obj_id, remote_obj_id in mapping:
                if None in (local_obj_id, remote_obj_id):
                    continue
                obj_map[local_obj_id] = remote_obj_id
        return obj_map

    @classmethod
    def get_sync_local_queryset(cls, model):
        if cls.sync_local_queryset:
            return cls.sync_local_queryset
        return model.objects.all()

    @classmethod
    def get_sync_remote_queryset(cls):
        if cls.sync_remote_queryset:
            return cls.sync_remote_queryset
        return cls.get_sync_remote_model().objects.all()

    @classmethod
    def get_sync_engine_class(cls):
        return cls.sync_engine_class

    @classmethod
    def get_sync_remote_model(cls):
        return cls.sync_remote_model

    @classmethod
    def get_sync_engine(cls):
        return cls.get_sync_engine_class()(
            local_model=cls, remote_model=cls.get_sync_remote_model()
        )

    @classmethod
    def _sync(cls, local_objects, *, pretend=False, partial=False):
        remote_objects = cls.get_sync_remote_queryset()
        obj_map = cls.get_sync_objects_map(local_objects, remote_objects)
        remote_data = dict(map(cls.get_sync_remote_data, remote_objects))
        local_data = {}
        for lo in local_objects:
            local_data.update(lo.get_sync_local_data_multi())
        if partial:
            remote_data = {
                remote_obj_id: remote_data
                for remote_obj_id, remote_data in remote_data.items()
                if remote_obj_id in obj_map.values()
            }
        with cls.get_sync_engine() as sync_engine:
            return sync_engine.sync(local_data, remote_data, obj_map, pretend=pretend)

    @classmethod
    def partial_sync(cls, local_objects, *, pretend=False):
        objs = list(map(cls, local_objects))
        return cls._sync(objs, pretend=pretend, partial=True)

    @classmethod
    def sync_all(cls, model, *, pretend=False):
        objs = list(map(cls, cls.get_sync_local_queryset(model)))
        return cls._sync(objs, pretend=pretend, partial=False)
