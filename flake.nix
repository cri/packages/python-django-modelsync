{
  description = "One way sync between Django models";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    futils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, futils } @ inputs:
    let
      inherit (nixpkgs) lib;
      inherit (lib) recursiveUpdate;
      inherit (futils.lib) eachDefaultSystem defaultSystems;

      nixpkgsFor = lib.genAttrs defaultSystems (system: import nixpkgs {
        inherit system;
        overlays = [ self.overlay ];
      });

      poetryArgs = pkgs: {
        projectDir = self;
        src = self;

        overrides = pkgs.poetry2nix.overrides.withDefaults (self: super: {
          black = null;
          pre-commit = null;
          virtualenv = null;
        });

        meta = with lib; {
          inherit (self) description;
          maintainers = with maintainers; [ risson ];
        };
      };

      anySystemOutputs = {
        overlay = final: prev: {
          modelsync = final.poetry2nix.mkPoetryApplication (poetryArgs final);
        };
      };

      multipleSystemsOutputs = eachDefaultSystem (system:
        let
          pkgs = nixpkgsFor.${system};
        in
        {
          devShell = pkgs.mkShell {
            buildInputs = with pkgs; [
              git
              nixpkgs-fmt
              poetry
              (poetry2nix.mkPoetryEnv (removeAttrs (poetryArgs pkgs) [ "meta" "src" "postInstall" ]))
              pre-commit
            ];
          };

          packages = {
            inherit (pkgs) modelsync;
          };
          defaultPackage = self.packages.${system}.modelsync;
        });
    in
    recursiveUpdate multipleSystemsOutputs anySystemOutputs;
}
